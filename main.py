import streamlit as st

from openai_rag_service import OpenAIRAGService

st.set_page_config(page_title='PDF Chat Bot | OpenAI')

if "messages" not in st.session_state:
    st.session_state.messages = []


def main():
    st.sidebar.title('Ask questions to your PDF using OpenAI')

    # Upload PDF file
    pdf = st.sidebar.file_uploader('**Upload your PDF**', type='pdf')
    if pdf is not None:
        rag_service = OpenAIRAGService(pdf_file=pdf)

        for message in st.session_state.messages:
            with st.chat_message(message["role"]):
                st.markdown(message["content"])

        # Question
        query = st.chat_input('Ask your questions here')

        # Ask
        if query is not None:
            with st.chat_message("user"):
                st.markdown(query)
            st.session_state.messages.append({"role": "user", "content": query})

            (result, cb) = rag_service.generate_response(
                query=query,
                use_question_decomposition=True,
                use_individual_answers=True
            )
            with st.chat_message("assistant"):
                st.markdown(result)
            st.session_state.messages.append({"role": "assistant", "content": result})
            st.write(cb)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
