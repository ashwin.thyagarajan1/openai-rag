import os

from PyPDF2 import PdfReader
from langchain.retrievers import ContextualCompressionRetriever
from langchain.retrievers.document_compressors import FlashrankRerank
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.callbacks import get_openai_callback
from langchain_community.vectorstores import FAISS
from langchain_core.documents import Document
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import ChatPromptTemplate
from langchain_openai import OpenAIEmbeddings, OpenAI
from streamlit.runtime.uploaded_file_manager import UploadedFile

from question_decomposition_rag_service import QuestionDecompositionRAGService


class OpenAIRAGService:
    def __init__(self, pdf_file: UploadedFile):
        text = self._get_pdf_text(pdf_file=pdf_file)

        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=1500,
            chunk_overlap=200,
            length_function=len
        )
        chunks = text_splitter.split_text(text=text)
        self.vector_store = self._set_vector_store(pdf_file=pdf_file, chunks=chunks)

    @staticmethod
    def _set_vector_store(pdf_file: UploadedFile, chunks: list[str]):
        embeddings = OpenAIEmbeddings()
        store_name = os.path.join('stores', pdf_file.name[:-4])

        if os.path.exists(os.path.join(store_name, 'index.pkl')):
            vector_store = FAISS.load_local(
                store_name,
                embeddings,
                allow_dangerous_deserialization=True
            )
        else:
            vector_store = FAISS.from_texts(
                chunks,
                embedding=embeddings
            )
            vector_store.save_local(store_name)
        return vector_store

    @staticmethod
    def _get_pdf_text(pdf_file: UploadedFile):
        pdf_reader = PdfReader(pdf_file)
        text = ""
        for page in pdf_reader.pages:
            text += page.extract_text()
        return text

    @staticmethod
    def __simple_rag_response(query: str, llm: OpenAI, docs: list[Document]):
        prompt_template = """Use the following context as your learned knowledge, inside <context></context> XML tags.
                <context>
                    {context}
                </context>

                When answer to user:
                - If you don't know, just say that you don't know.
                Avoid mentioning that you obtained the information from the context.
                And answer according to the language of the user's question.

                Given the context information, answer the query.
                Query: {question}"""
        prompt = ChatPromptTemplate.from_template(prompt_template)
        chain = prompt | llm | StrOutputParser()
        response = chain.invoke({"context": docs, "question": query})
        return response

    def generate_response(self,
                          query: str,
                          use_question_decomposition: bool = False,
                          use_individual_answers: bool = False):
        llm = OpenAI(temperature=0)
        docs = self.vector_store.similarity_search(query=query, k=3)
        with get_openai_callback() as cb:

            if use_question_decomposition:
                retriever = self.vector_store.as_retriever(search_kwargs={"k": 3})
                compressor = FlashrankRerank()
                compression_retriever = ContextualCompressionRetriever(
                    base_compressor=compressor,
                    base_retriever=retriever
                )
                question_decomposition_rag_service = QuestionDecompositionRAGService(
                    llm=llm,
                    retriever=compression_retriever,
                    docs=docs
                )
                response = question_decomposition_rag_service.generate_response(
                    query=query,
                    use_individual_answers=use_individual_answers
                )
            else:
                response = self.__simple_rag_response(
                    query=query,
                    llm=llm,
                    docs=docs
                )
            return response, cb
